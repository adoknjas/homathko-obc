# README

## Before You Start

The physical OBC is designed to be used as a stand alone development platform powered by USB and flashed/debugged over the cTI 20-pin JTAG. The current revision of the Homathko/ORCAASat OBC is designed to only be flashed over the JTAG interface. The USB port acts as a terminal interface to the OBC but the OBC cannot be flashed over it. The USB port also powers the OBC when the OBC is powered over the backplane. Powering over the backplane depends on what other system is also populated on the backplane at the same time as the OBC. Some development modules and/or the power system will power the OBC over the backplane. It is important to note that you should not allow the OBC to be powered by both the USB and the backplane simultaneously.

## Hardware Overview

### Power
VSYS can range from 3.4 V to 5.1 V.

### Power Sources
The OBC can be powered from the USB port or the backplane. It is important to note that the OBC should not be powered by both at the same time. The data lines from the USB are always connected, but the OBC can be configured into three different modes of operation in regards to how it is powered.

1. Standalone mode: Standalone mode is designed to power the OBC when the OBC is not using a backplane. This is mode of power is mainly for development and is not meant for testing or integration. Note when running in standalone mode, the USB port also powers an internal 3V3 regulator that is not used in other modes.![Standalone Mode][standalone mode] 

2. Charge mode: Charge mode allows for the USB port to provide power back to the power system to allow the batteries to charge while integrated into the backplane. This mode is specifically meant for environmental testing. It is important to note that the deployment switches must not be activated and/or disconnected or the ground port will supply the OBC with power over the V_SYS rail and this strictly prohibited as outlined in CSDC and NanoRacks requirements documentation. Charge mode should only ever be used when the satellite is integrated into either the NanoRacks CubeSat Deployer (NRCSD) or P-Pod CubeSat Deployer. ![Charge Mode][charge mode]

3. Flight mode: Flight mode disconnects any ability to supply power from the USB port. This mode is designed for simulated flight environment. The USB port does not supply any power to the satellite but a USB cable can remain connected to receive telemetry. Note the the 3V3 rail is powered by the power system and the 3V3 electronics are not powered. ![Flight Mode][flight mode]

Regardless of the mode of operation, the USB port has ESD protection and 500mA current limiting that is powered off the USB power when a USB cable is inserted. Even if the OBC is in flight mode, the USB circuitry is always behind ESD and surge protection. It is important to note that the USB to UART interface converter is powered from the USB port.

### JTAG Information

The OBC uses a cTI 20 pin header, this header is directly compatible with the XDS110 debug probe.

[Here](http://software-dl.ti.com/ccs/esd/xdsdebugprobes/emu_xds_target_connection_guide.html#introduction) is an excellent resource for the debugging TI MCUs.

[XDS Target Connection Guide](http://software-dl.ti.com/ccs/esd/xdsdebugprobes/emu_xds_target_connection_guide.html), a great reference for schematic capture and PCB layout with a TI MCU and a debug header. More in-depth [PDF](http://www.ti.com/lit/ug/spru655i/spru655i.pdf).

## Software Overview

## TODO

__March 1, 2018:__ The LIN interface is unused, but will probably be used to interface the TMS570 and DSP for the communications system. This LIN interface should be broken out to a connector.

__March 1, 2018:__ Add ADCS magnetometer to the schematic.

__March 1, 2018:__ Re-do the temperature sensor schematic for a sensor that runs off a 3.0 V reference better.




delete and re-add all 4.7 uF caps and replace with the new 4.7 uF caps in the 0805 form factor.

## Log

Testing GitLab, sorry

[standalone mode]: Documentation/Images/OBC_Power_A.png
[charge mode]: Documentation/Images/OBC_Power_B.png
[flight mode]: Documentation/Images/OBC_Power_C.png

# OBC and Software Architecture
ORCAASat will only have one microcontroller. The microcontroller executes the following:

* Communications
  * OBC will implements a communications stack
  * OBC will use TCP + IP + TLS 2.0
* Telemetry
  * OBC will obtain sensor data using interfaces
  * Sensor data will be logged or sent back to the ground station
* Payload control
  * OBC control the execution of the payload at a specific time
  * The OBC will control the attitude
  * The OBC will record the attitude at that specific time
* Power control
  * OBC will monitor power consumption to prevent brown outs
* Data handling
  * OBC will process all data coming from all interfaces
  * Processing temperature sensors
  * Filtering sensor data using Kalman filter
* Timekeeping
  * OBC will sync the time using GPS
* Memory integrity
  * OBC will protect all read/write memory from errors


## Features:
  * Static memory allocation
  * 40 microsecond timing accuracy
  * Software real time clock in seconds and microseconds
  * CRC FreeRTOS queue protection
  * Automatic scheduling of tasks and timers
  * Finite State Machine
  * Watchdog Task to monitor individual tasks and timers
  * Mission planning using orbit paths
  * Comprehensive unit tests and system tests  
  
  
## PCB Physical Architecture: 
*	PCB Design is final (barring any unforeseen drastic changes)
*	Back plane is for running interconnects between systems (e.g. Power needs to be connected to every system)
*	I2C is for the Power system, and since the I2C is prone to error already, no other system should use it.
*	Comms is copy-pasted from both EcoSAT III and Bros' OBC
*	Solar Panels will physically plug into Power
*	Regarding General Purpose I/O (GPIO), all of the analog pins won't be connected to the OBC, would need to physically solder if this is desired
*	Control has 4 enable interfaces which are for the OBC to check is systems are enabled
*	Lots of pins are doubled up on the back plane

## OBC Specific Info:
*	Comms is mapped right through the back plane
*	LIN will be used for Digital Signal Processing
*	SCI will be used for USB
*	OBC Analog will be used for current sensing
*	Temperature sensing and Magnetometers will be used but not on Analog
*	There is an external Watchdog with hardware set timers as well as the watchdogs on the TMS570
*	JTAG header can reset the chip, but there are mutliple TMS reset procedures
*	Jumper is used for Power
*	No matter what state the jumper is in you still receive data
*	Always use the default first when working with the OBC
*	Designed so this board can be used instead of the Development Board/Microcontroller, as well as for testing
  
  
# SMBus vs. i2c Meeting
## March 22, 2018 

## Meeting goals:
*	SPI photodiode
*	i2c SMBus 
*	Schedule SciSend issue

## Notes: 
*	SciSend issue fixed (SciInit needs to be before ScheduleInit)
*	Have an understanding of SMBus but no hardware to work with for practice
*	SPI code done, waiting on real photodiodes for testing

## For next time: 
*	Merge current code into master test code (Ben)
*	Figure out how to staty busy as we wait for SPI baterry physical hardware
*	Start work on actual OBC

# April 30 Deadline Meeting/ Sprint 2 Work Session 
## March 15, 2018 

## Meeting goals: 
*	Reallocate resources/timeline to compensate for updated timeline 
*	Start work on SPI comms

## Notes:
*	Nicole is doing GPS
* 	Ryan and Sam are doing Scheduling 
*	Julian, Kai and Patrick are doing Battery
*	Ben and Jose are doing Payload

## For next time:
*	Update readme
*	Start progress on all submodules
* 	Organize git into submodules

# Sprint 2 Work Session
## March 10, 2018  

## Meeting goals: 
1. Plan Sprint 2

## Notes:
*	Scheduling: Sam and Ryan 
*	Magnetometer: Ben and Jose
*	Battery: Ben, Kai and Julian 

## For next meeting: 
*	Figure out i2c comms using magnetometer
*	Figure out what scheduling architecture we want to use


# Sprint 1 Work Session
## March 6, 2018

## Meeting goals: 
1. Convert read/write capabilities to external debugger
2. Finish USB/UART comms
3. Check up on sprint progress (scheduling, comms, i2c, GPS)

## Notes:
*	Ext debugger finished
*	USB/UART finished
*	GPS found 

## For next meeting: 
*	Find i2c magnetometer
*	Contact GPS supplier for info on unlocking [Jose]
*	Scheduling needs work
*	Figure out what role our battery software will play (Do we just read? Do we need to interpret to any degree?)
*	Talk to Bryce/Alex about next steps for Sprint 2 (Scheduling and telemetry make sense to do?)

# Sprint 1 Final Mid-sprint Meeting
## March 4, 2018 

## Meeting goals: 

1. Determine pins for i2c battery telemetry
2. Check up on sprint progress (battery, GPS) 
3. Merge current work into main repo
4. Update readme to explain function of OBC better, including backplane/OBC pins
5. Find Source code for HAMSF custom functions 

## Notes: 
*	HAMSFprint def'n is in DebugUART.c
*	Jose thinks he has found a GPS
*	Use "scilinREG" for LS12 board rather than "sciREG1". This is because the pins are muxed differently: https://e2e.ti.com/support/microcontrollers/hercules/f/312/p/668850/2461399#2461399

## For next meeting: 
*	No luck on debugger today (no time), will need to do that on Tuesday
*	Start progress on scheduling
*	I2C usage needs to be researched further, Kai and Julian will focus exclusively on that
*	Confirm GPS choice
*	Will merge repo into ORCAASat repo once we have debugger done

# Sprint 1 Mid-Sprint Meeting
## Feb 24, 2018

## Meeting goals: 
1.	Establish read (hello world) and write (flashing light) capabilities to the board
2.	Establish direction for battery/timing systems 
3.	Slides for Tibor presentation

## Notes: 
*	Patrick can now flash lights and read from board using the terminal 
*	External XDS work is delayed due to missing header
*	No luck on GPS search thus far 
*	We only need battery telemetry, not management

## For next meeting: 
*	[Julian and Kai] Look into I2c comms for the battery telemetry 
*	[Ben and Patrick] Ensure smooth transition to external debugger
*	[Jose] Keep looking for the GPS
*	[Sam and Ryan] Start work on a scheduler that fires the lights on the board

# Sprint 1 Planning
## February 17, 2018

## Meeting goals: 
1.	Establish UART/USB comms with board
2.	Assign major tasks for next few months to team members
3.	Plan first sprint

## Notes: 
*	Sam/Patrick figured out how to display board info on terminal, meaning we have established baseline functionality for stress testing in May.
*	Patrick’s solution in implemented in Visual studio, we want to figure out if it can be implemented in CCS
*	Still waiting on external debugger (XDS110), but can start working on ability to write to board using internal debugger (also an XDS110)
*	First sprint: GPS source, basic two-way debugger functioning, battery polling, basic scheduling/timing 

## For next meeting: 
*	[Patrick] Figure out if we can continue to *just* use the CCS IDE for development if we’ll need to integrate other coding tools (visual studio etc) for our debugger
*	[Julian, Kai, Ben] Internal battery management research and implementation. The power system handles most of the decision making about priority of tasks, but we still need to be able to poll the system to read our power input/ouput level and transmit it to the ground station, similar to our telemetry. 
*	[Jose] Take a look into an alternative for the Novatel OEMStar that we can actually purchase
*	[Sam, Ryan] Start working on internal timing/scheduling. Figure out what hardware on the board can be used and how to use. Establish a basic scheduling program to blink a light. 



# Repo review/Project goals
## February 3, 2018

## Meeting goals: 
1.	Review Brosnan’s Repo 
2.	Review major semester goals 
3.	Establish general schedule for semester 

## Notes: 
*	Should establish reading list on GitLab for Thursday 
*	Need a comms debugger *TOP PRIORITY*
*	First sprint: Timer and onboard temp polling 
*	Scheduler will need to be established before implementing ext. systems readings (adcs etc)
*	Consider writing a scheduler to poll temp/power readings 

## For next meeting: 
*	Read through the following modules: Time, SystemBus, Communications, Startup 


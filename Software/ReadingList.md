# ORCAASat Reference List 

## General Software Information

FreeRTOS Website: https://www.freertos.org/

FreeRTOS Ultimate Reference Guide: https://www.freertos.org/Documentation/FreeRTOS_Reference_Manual_V10.0.0.pdf

Using PuTTy: http://store.chipkin.com/articles/using-putty-for-serial-com-connections-hyperterminal-replacement


## General Hardware Information

Explanation of the debugging process: [OBC Debugger] (https://git.uvsd.ca/orcaasat/obc/ORCAASat_OBC_OBC/blob/master/Software/AttachedFiles/DebuggerDiagram.pdf)

[TMS570 Architecture] (https://git.uvsd.ca/orcaasat/obc/ORCAASat_OBC_OBC/blob/master/Software/AttachedFiles/TMS570SoftwareArchitecture.png)

TMS570LC43 Technical Reference Manual: http://www.ti.com/lit/ug/spnu563/spnu563.pdf

TMS570LC43 References: http://www.ti.com/product/TMS570LC4357/technicaldocuments	

TMS570LC43 Pin Diagrams: [Getting started with the Launchpad](https://git.uvsd.ca/orcaasat/obc/ORCAASat_OBC_OBC/blob/master/Software/AttachedFiles/LaunchpadStarter.pdf)

TMS570LC43 Pin Diagrams (More in depth): [Launchpad schematics](https://git.uvsd.ca/orcaasat/obc/ORCAASat_OBC_OBC/blob/master/Software/AttachedFiles/LaunchpadSchematics.pdf)

TMS570LS12 User Guide : http://www.ti.com/lit/ug/spnu613/spnu613.pdf

TMS570LS12 Pin Diagrams (More in depth): [Launchpad schematics](http://processors.wiki.ti.com/images/c/c1/LAUNCHXL2_TMS57012_RM46_REVA.pdf)

UART/USB Breakout Board Information: http://www.ftdichip.com/Support/Documents/DataSheets/Modules/DS_UMFT201_220_230XB.pdf

Magnetometer Breakout Board Datasheet (Page 25 has info on i2c comms): https://www.invensense.com/wp-content/uploads/2015/02/MPU-9250-Datasheet.pdf


## SCI comms

[Reading from the board to CCS terminal](https://training.ti.com/hercules-how-tutorial-using-sci-uart-communication) (slightly different than our board, see above architecture documentation) 

Setting up the SCI for UART communication: https://www.youtube.com/watch?v=PpalANwuzIo

TMS570LS12 SCI/UART Setup: http://www.ti.com/lit/an/spna124a/spna124a.pdf


## I2C comms

I2C basics: https://learn.sparkfun.com/tutorials/i2c


## SPI comms



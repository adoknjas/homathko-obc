# ORCASF

ORCAASat Software Framework is an abstraction layer for the software written on ORCAASat. The target development board is TMS570LS12, using FreeRTOS and TMS570 HAL.

## Repo Structure

* ORCASF
* [readme.md](readme.md) ORCASF main documentation
* [Requirements](Requirements.md)  Requirements of the whole ORCASF
* Module
	* [ModuleName]
		* [readme.md](readme.md) Module documentation
		* [ModuleName].h  Source code header file of module
		* [ModuleName].c  Source code C file of module
		* [ModuleName]Test.h  Unit test for module header file
		* [ModuleName]Test.c  Unit test for module
		* [ModuleName]Example.c  Example code on how to use module
* SystemsTest
    * [readme.md](readme.md) Document on tests used
    * ORCASFTest.h Whole system testing header file
    * ORCASFTest.c Whole system testing
    * ORCASFExample.c Example code for using ORCASF
* Include
    * FreeRTOSConfig.h   Config files for FreeRTOS
* Source
    * HL_sys_main.c      Main entry point
  
## Modules
ORCASF is made out of many different modules. Each module is a collection of APIs and data structures. Every module has a specific task.

| Name              | Description                                                                                                 | Status     |
|-------------------|-------------------------------------------------------------------------------------------------------------|------------|
| SystemBus         | Intertask communications.                                                                                   | Incomplete |
| Time              | Tracking and setting time.                                                                                  | Incomplete |
| Communications    | Manages the communications stack. Contacts the ground station.                                              | Incomplete |
| GPS               |                                                                                                             | Incomplete |
| Magnetometer      |                                                                                                             | Incomplete |
| Gyroscope         |                                                                                                             | Incomplete |
| ReactionWheels    |                                                                                                             | Incomplete |
| Magnetorquers     |                                                                                                             | Incomplete |
| SunSensor         |                                                                                                             | Incomplete |
| HorizonSensor     |                                                                                                             | Incomplete |
| LASER             | Controls execution of the LASER.                                                                            | Incomplete |
| IntegratingSphere | Monitors illuminace of LASER in the integrating sphere.                                                     | Incomplete |
| SelfDiagnostics   |                                                                                                             | Incomplete |
| KalmanFilter      |                                                                                                             | Incomplete |
| Temperature       |                                                                                                             | Incomplete |
| FileSystem        | Protects data from radiation effects.                                                                       | Incomplete |
| Logging           | Logs all data in the system.                                                                                | Incomplete |
| Watchdog          | Monitors all tasks to check if they are function correctly.                                                 | Incomplete |
| Power             | Monitors the power flow of the ORCAASat. Determines if there is enough energy to keep ORCAASat functioning. | Incomplete |
| Database          | Keeps track of sensor failures and events.                                                                  | Incomplete |
| AttitudeControl   | Allows pointing of LASER.                                                                                   | Incomplete |
| Detumble          | Prevents satellite from spinning too fast.                                                                  | Incomplete |
| Scheduler         | Executes finite state machine and schedules tasks.                                                          | Incomplete |
| Misc              | Other APIs calls.                                                                                           | Incomplete |
| Bootloader        | Loads FreeRTOS                                                                                              | Incomplete |
| MissionPlanner    | Plans the execution of all systems                                                                          | Incomplete |


## Getting Started
See [Getting Started](GettingStarted.md)

## Reference Guides
See [Reading List] (ReadingList.md)

## Meeting Notes
See [Meeting Notes] (MeetingNotes.md)

## Hardware Design
See [OBC Design](https://git.uvsd.ca/orcaasat/obc/ORCAASat_OBC_OBC/blob/master/Documentation/ORCAASat_OBC.pdf)

See [Backplane Design] (https://git.uvsd.ca/orcaasat/obc/ORCAASat_OBC_OBC/blob/master/Documentation/ORCAASat_PCB.pdf)

See [OBC Leads Design Review Notes](https://git.uvsd.ca/orcaasat/obc/ORCAASat_OBC_OBC/blob/master/Software/OBC%20and%20PCB%20Leads%20Design%20Review%20Notes)

## Software Architecture
See [Architecture] (Architecture.md) 

.
